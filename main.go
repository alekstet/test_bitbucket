package main

import "fmt"

func Substract(x, y int) int {
	return x - y
}

func Add(x, y int) int {
	return x + y
}

func main() {
	x := Add(5, 10)
	x1 := Substract(10, 5)
	fmt.Println(x, x1)
}