package main

import "testing"

func TestAdd(t *testing.T) {
    v := Add(10, 5)
    if v != 15 {
        t.Error("Expected 15, got ", v)
    }
}

func Test_Substract(t *testing.T) {
    v := Substract(10, 5)
    if v != 5 {
        t.Error("Expected 5, got ", v)
    }
}